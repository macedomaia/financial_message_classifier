from keras.models import model_from_json
import nltk
import numpy as np
import os

from myutils import nltk_util as nltk_u
from myutils import distributional_util as distr_u
class FinancialSentenceSentiment:
    def __init__(self):
        pass

    def getSentiFeatures(self,sentence,max_sentence_size):
        in_tuple = (1,max_sentence_size,2)
        senti = np.empty(in_tuple, dtype= 'float')
        senti.fill(-1.0)
        senti[0] = nltk_u.get_sentwordnet_sentence_scores_local(nltk.word_tokenize(sentence),max_sentence_size)
        return senti

    def ordering_vector(self,vector):
        ordered_vector = None
        if not (vector is None):
            ordered_vector = [0] * 300
            for lem in vector:
                ordered_vector[int(lem)] = vector[lem]

        return ordered_vector

    def get_vector(self,corpus, word):
        import requests
        import json
        import operator

        url = 'http://alphard.fim.uni-passau.de:8996/vectors'

        terms = [word]

        data = {'corpus': corpus,
                'model': 'W2V',
                'language': 'EN',
                'terms': terms}

        headers = {
            'accept': "application/json",
            'content-type': "application/json",
            'cache-control': "no-cache"
        }

        res = requests.post(url, data=json.dumps(data), headers=headers)
        res.raise_for_status()
        vector = self.ordering_vector(res.json()['terms'][word])
        return vector

    def getW2VVector(self,sentence,w2v_corpus,max_sentence_size):
        #normalized_sentence = distr_u.normalize_sentence(sentence[0])
        tokenized_sentence = distr_u.get_sent_tokenized(sentence[0])
        sentence = distr_u.get_distributional_vector_sentences([tokenized_sentence],w2v_corpus,max_sentence_size)


        return sentence


    def get_outcome(self,sentence, w2v_model, max_sentence_size, model):
        word_vectors = self.getW2VVector(sentence, w2v_model, max_sentence_size)
        senti_vectors = self.getSentiFeatures(sentence, max_sentence_size)
        main = model.predict_classes([word_vectors,senti_vectors], verbose=0)
        return main[0][0]


    def get_model(self):
        json_file = open(os.path.join(os.path.dirname(__file__),'../models/stock_arch_distant.json'), 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)

        loaded_model.load_weights(os.path.join(os.path.dirname(__file__),'../models/stock_weights_distant.hdf5'))
        loaded_model.compile(
            loss='binary_crossentropy',
            metrics=['accuracy'],
            optimizer='adam')
        return loaded_model

if __name__ == '__main__':
    predictor_object = FinancialSentenceSentiment()
    model = predictor_object.get_model()
    sentence = "@CGrantWSJ You highlight the REAL risk to the $VRX/spec pharma businesses. Payers who won't pay anymore! $ENDP $HZNP $MNK"
    max_sentence_size = 30
    w2v_model = 'googlenews300neg'
    outcome = predictor_object.get_outcome(sentence,w2v_model,max_sentence_size,model)
    print("Outcome: ",outcome)