__author__ = 'macedo'

from util_files.nltk_util import get_lemmatized_tokens,get_sent_tokenized, get_pos_tags, penn_to_wn
from util_files.string_util import normalize_sentence
import util_files.nltk_util as nltk_u
import util_files.string_util as string_u
import util_files.cosine as cosine
from sklearn.preprocessing import LabelBinarizer
from gensim import models
#from gensim import models
import nltk
import numpy as np
import re
import os
current_project_path = os.path.dirname(os.path.realpath(__file__)) + "/../"
STOPLIST= """
a about above across afterwards again against all almost alone along already also although always am among amongst amoungst amount an and another any a$
became because become becomes becoming been before beforehand behind being below beside besides between beyond bill both bottom but by call can
cannot cant co computer con could couldnt cry de describe
detail did didn do does doesn doing don done down due during
each eg eight either eleven else elsewhere empty enough etc even ever every everyone everything everywhere except few fifteen
fify fill find fire first five for former formerly forty found four from front full further get give go
had has hasnt have he hence her here hereafter hereby herein hereupon hers herself him himself his how however hundred i ie
if in inc indeed interest into is it its itself keep last latter latterly least less ltd
just
kg km
made make many may me meanwhile might mill mine more moreover most mostly move much must my myself name namely
neither never nevertheless next nine no nobody none noone nor not nothing now nowhere of off
often on once one only onto or other others otherwise our ours ourselves out over own part per
perhaps please put rather re
quite
rather really regarding
same say see seem seemed seeming seems serious several she should show side since sincere six sixty so some somehow someone something sometime sometime$
than that the their them themselves then thence there thereafter thereby therefore therein thereupon these they thick thin third this those though thre$
until up unless upon us used using
various very very via
was we well were what whatever when whence whenever where whereafter whereas whereby wherein whereupon wherever whether which while whither who whoever$
your yours yourself yourselves
"""

def get_finance_terms(file_path):
    word_list = []
    with open(file_path, 'r') as file:
        for line in file:
            line = re.sub('\([0-9]+\)', '', line.lower().strip())
            line = re.sub('[^a-zA-Z\s_]+', '', line)
            word_list.append(line)
    return word_list

def normalize_finance_terms(sentence):
    sentence = re.sub('\([0-9]+\)', '', sentence.lower().strip())
    sentence = re.sub('[^a-zA-Z\s_]+', '', sentence)
    return sentence

def get_stoplist():
    return [w for w in STOPLIST.split()]




def get_tokenized_sentences_and_classes(sentences):
#    tokenized_sentences = []
   
#    classes = []
#    max_sentence_size = 0
#    for i,sentence in enumerate(sentences):
#        splited_line = normalize_sentence(sentence).split("\t")
    #if i > 2000:
    #    break
#       print(splited_line)
#        if len(splited_line) == 2:
#            tokenized_sentence = get_lemmatized_tokens(splited_line[0])
#            tokenized_sentence = splited_line[0].split()
#            tokenized_sentences.append(tokenized_sentence)
#            if max_sentence_size < len(tokenized_sentence):
#                    max_sentence_size = len(tokenized_sentence)
#            classes.append(splited_line[1])
    tokenized_sentences = []
    classes = []
    max_sentence_size = 0
    print("sentence amount: "+str(len(sentences)))
#    with open('/home/sousa/stock_datased_balanced.txt', 'a') as f:
#    f.write('hi there\n') # python will convert \n to os.linesep
#f.close() # you can omit in most cases as the destructor will call it
    for i,sentence in enumerate(sentences):
#         print(str(i))
#         print(sentence)
#            f.write(sentence[0]+'\t\t'+sentence[1]+'\n')
         normalized_sentence = normalize_sentence(sentence[0].lower())
#         if i > 1000:
#             break
#         print(splited_line)

         tokenized_sentence = get_sent_tokenized(normalized_sentence)
#        tokenized_sentence = normalized_sentence.split()
         tokenized_sentences.append(tokenized_sentence)
         if max_sentence_size < len(tokenized_sentence):
             max_sentence_size = len(tokenized_sentence)
         classes.append(sentence[1])
            #f.write(sentence[0]+'@'+sentence[1]) # python will convert \n to os.linesep
#    f.close()
    
    return tokenized_sentences, classes, max_sentence_size


def get_tokenized_sentences_and_classes_investopedia(sentences):
#    tokenized_sentences = []
   
#    classes = []
#    max_sentence_size = 0
#    for i,sentence in enumerate(sentences):
#        splited_line = normalize_sentence(sentence).split("\t")
#    if i > 100:
#        break
#       print(splited_line)
#        if len(splited_line) == 2:
#            tokenized_sentence = get_lemmatized_tokens(splited_line[0])
#            tokenized_sentence = splited_line[0].split()
#            tokenized_sentences.append(tokenized_sentence)
#            if max_sentence_size < len(tokenized_sentence):
#                    max_sentence_size = len(tokenized_sentence)
#            classes.append(splited_line[1])
    tokenized_sentences = []
    classes = []
    max_sentence_size = 0
#    with open('/home/sousa/stock_datased_balanced.txt', 'a') as f:
#    f.write('hi there\n') # python will convert \n to os.linesep
#f.close() # you can omit in most cases as the destructor will call it
    for i,sentence in enumerate(sentences):
         print(str(i))                 
#            f.write(sentence[0]+'\t\t'+sentence[1]+'\n')
#         normalized_sentence = normalize_sentence(sentence[0])

#         if i > 500:
#             break
#         print(splited_line)

         tokenized_sentence = get_investopedia_terms(sentence[0])
#         print(tokenized_sentence)
         tokenized_sentence = tokenize(tokenized_sentence)
         tokenized_sentences.append(tokenized_sentence)
         if max_sentence_size < len(tokenized_sentence):
             max_sentence_size = len(tokenized_sentence)
         classes.append(sentence[1])
            #f.write(sentence[0]+'@'+sentence[1]) # python will convert \n to os.linesep
#    f.close()
    
    return tokenized_sentences, classes, max_sentence_size


def isAlphanumeric(token):
    pattern = "[A-Za-z]"
    match = re.findall(pattern, token)
    return True if len(match) > 0 else False


def load_finance_dictionary(files_path):
    dictionary = []
    for file_path in files_path:
        with open(file_path,'r') as lines:
            dictionary += [word.replace("\n","") for word in get_finance_terms(file_path)]
    return dictionary

def get_investopedia_terms(sentence):
    
    path= [current_project_path + "files/finance_terms.txt"]
    investopedia_terms = load_finance_dictionary(path)
    new_sentence = sentence[:].lower()
#    print(sentence)
#    new_sentence = sentence.lower()
    for term in investopedia_terms:
        if len(term.split()) >= 2:
            new_sentence = re.sub(term,term.replace('\s','_'),new_sentence)
    return new_sentence

def tokenize(text, dictionary=None):
    #path_investopedia = "../data/"
    text = get_investopedia_terms(text)
    text = re.sub("(\$[A-Za-z]+)","",text)
    text_tokenize = nltk.word_tokenize(normalize_finance_terms(text))
    if dictionary is None: 
        tokenized_sentence = [token for token in text_tokenize]
    else:
        tokenized_sentence = [token for token in text_tokenize if token in dictionary]
    return [token for token in tokenized_sentence if token not in get_stoplist() and isAlphanumeric(token)]
#    return [token for token in tokenized_sentence]



def get_lines(files):
    lines = []
    for file in files:
        with open(file) as f:
            for line in f:
                lines.append(line)
    return lines

def generate_w2v_model(sentences):
    return models.Word2Vec(sentences, min_count=1,size=300)

def ordering_vector(vector):
    ordered_vector = None
    if not(vector is None):
        ordered_vector = [0] * 300
        for lem in vector:
            ordered_vector[int(lem)] = vector[lem]

    return ordered_vector


def get_vector(corpus, word):
    import requests
    import json
    import operator

    url = 'http://alphard.fim.uni-passau.de:8996/vectors'

    terms = [word]

    data = {'corpus': corpus,
            'model': 'W2V',
            'language': 'EN',
            'terms': terms}

    headers = {
            'accept': "application/json",
            'content-type': "application/json",
            'cache-control': "no-cache"
        }

    res = requests.post(url, data=json.dumps(data), headers=headers)
    res.raise_for_status()
    vector = ordering_vector(res.json()['terms'][word])
    return vector

def get_cosine_vector_sentence(tokenized_sentence,w2v_corpus,max_sentence_size,vector_comparison):
    in_tuple = (max_sentence_size,len(vector_comparison))
    new_vector = np.empty(in_tuple, dtype= 'float')
    new_vector.fill(0.0)
    

    for j,word in enumerate(vector_comparison):
        word_vector = get_vector(w2v_corpus,word)
#        print(word,str(word_vector))
        for i,token in enumerate(tokenized_sentence):
           token_vector = get_vector(w2v_corpus,token)
           
           if max_sentence_size <= i:
               return new_vector
           if not(token_vector is None):
               new_vector[i][j] = cosine.abs_cosine_similarity(word_vector,token_vector)
               
    return new_vector

def get_cosine_vector_sentences(tokenized_sentences,w2v_corpus,max_sentence_size,vector_comparison):
    vectors =[]
    for sentence in tokenized_sentences:
        vectors.append(get_cosine_vector_sentence(sentence,w2v_corpus,max_sentence_size,vector_comparison))
    return np.array(vectors)

def get_cosine_vector_sentence_local(tokenized_sentence,w2v_model,max_sentence_size,vector_comparison):
    in_tuple = (max_sentence_size,len(vector_comparison))
    new_vector = np.empty(in_tuple, dtype= 'float')
    new_vector.fill(0.0)
    
    for j,word in enumerate(vector_comparison):
        word_vector = w2v_model[word]
        for i,token in enumerate(tokenized_sentence):
            if max_sentence_size <= i:
               return new_vector

            if token in w2v_model:
                vec = w2v_model[token]
                new_vector[i][j] = cosine.abs_cosine_similarity(word_vector,vec)
   
    return new_vector

def get_cosine_vector_sentences_local(tokenized_sentences,w2v_model,max_sentence_size,vector_comparison):
    vectors =[]
    for sentence in tokenized_sentences:
        vectors.append(get_cosine_vector_sentence_local(sentence,w2v_model,max_sentence_size,vector_comparison))
    return np.array(vectors)


def get_distributional_vector_sentence(tokenized_sentence,w2v_corpus,max_sentence_size):
    in_tuple = (max_sentence_size,300)
    w2v_tokenized_sentence = np.empty(in_tuple, dtype= 'float')
    w2v_tokenized_sentence.fill(-1.0)

    for i,token in enumerate(tokenized_sentence):
        if max_sentence_size <= i:
           return w2v_tokenized_sentence
        token_vector = get_vector(w2v_corpus,token)
        if not(token_vector is None):
           vec = token_vector
#           print(vec)
        else:
           vec = [-1.0] * 300
        w2v_tokenized_sentence[i] = vec
    return w2v_tokenized_sentence

def get_distributional_vector_sentence_local(tokenized_sentence,w2v_model,max_sentence_size):
    in_tuple = (max_sentence_size,300)
    w2v_tokenized_sentence = np.empty(in_tuple, dtype= 'float')
    w2v_tokenized_sentence.fill(-1.0)

    for i,token in enumerate(tokenized_sentence):
        if max_sentence_size <= i:
           return w2v_tokenized_sentence
        #token_vector = get_vector(w2v_corpus,token)
        if token in w2v_model:
#           print(token)
#           print("1")
           
           vec = w2v_model[token]
#           print(vec)
        else:
#           print(token)
#           print("0")
           vec = [-1.0] * 300
        w2v_tokenized_sentence[i] = vec
    return w2v_tokenized_sentence


def get_distributional_vector_sentences(tokenized_sentences,w2v_corpus,max_sentence_size):
    print("distributional sentences actived")
    print(len(tokenized_sentences))
    in_tuple = (len(tokenized_sentences), max_sentence_size,300)
    
    in_vectors = np.empty(in_tuple,dtype=np.float64)
    in_vectors.fill(-1.0)

    for i,tokenized_sentence in enumerate(tokenized_sentences):
        in_vectors[i] = get_distributional_vector_sentence(tokenized_sentence,w2v_corpus,max_sentence_size)
    return in_vectors

def get_distributional_vector_sentences_local(tokenized_sentences,w2v_model,max_sentence_size):
    print("distributional sentences actived")
    print(len(tokenized_sentences))
    in_tuple = (len(tokenized_sentences), max_sentence_size,300)
    
    in_vectors = np.empty(in_tuple,dtype=np.float64)
    in_vectors.fill(-1.0)

    for i,tokenized_sentence in enumerate(tokenized_sentences):
        in_vectors[i] = get_distributional_vector_sentence_local(tokenized_sentence,w2v_model,max_sentence_size)
    return in_vectors


def get_pos_sentences(sentences,max_sentence_size):
    in_tuple = (len(sentences), max_sentence_size,1)
    pos_sentences = np.empty(in_tuple,dtype=object)
    pos_sentences.fill(None)

    for i,sentence in enumerate(sentences):
        splited_line = normalize_sentence(sentence).split("\t")
        tokenized_sentence = get_lemmatized_tokens(splited_line[0])

        pos_sentence =  get_pos_tags(splited_line[0])
        for j,pos in enumerate(pos_sentence):
            pos_sentences[i][j] = penn_to_wn(pos_sentence[j])


        if len(tokenized_sentence) != len(pos_sentence):
                print("error")
                print(splited_line[0])
                print(len(tokenized_sentence))
                print(len(pos_sentence))

    return pos_sentences


def load_w2v_model(model_path, extension=None):
    if extension is "bin":
        return  models.Word2Vec.load_word2vec_format(fname=model_path,binary=True)
    return  model.Word2Vec.load_word2vec_format(fname=model_path,binary=False)

def minibatch_generator_cosine(X_train,y_train,batch_length,w2v_corpus,sentence_size,vector_comparison,further_feature_vector=None):

    amount_batchs = int(round(len(X_train)/batch_length))
    print("amount batch: ",str(amount_batchs))
    current_batch = 0
    while 1:
         if current_batch == amount_batchs:
             print("current batch: ",str(current_batch))
             train_set = []
             full_train_set = []
             print(str(current_batch*batch_length)+" "+str(len(y_train)))
             for elem in X_train[current_batch*batch_length:len(y_train)]:
                 train_set.append(get_cosine_vector_sentence(elem,w2v_corpus,sentence_size,vector_comparison))
             if further_feature_vector is None:
                 full_train_set = np.array(train_set)
             else:
                 batch_feature_vector = further_feature_vector[current_batch*batch_length:len(y_train)]
                 full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
             y =  y_train[current_batch*batch_length:len(y_train)]
             current_batch = 0
             print("y size: "+str(len(y_train[current_batch*batch_length:len(y_train)])))
             yield full_train_set, y

         for i in range(amount_batchs): # 1875 * 32 = 60000 -> # of training samples
            print("batch number: ",str(i))
            train_set = []
            full_train_set = []
            print(str(i*batch_length)+" "+str((i+1)*batch_length))
            for elem in X_train[i*batch_length:(i+1)*batch_length]:
                train_set.append(get_cosine_vector_sentence(elem,w2v_corpus,sentence_size,vector_comparison))
            if further_feature_vector is None:
                full_train_set = np.array(train_set)
            else:
                batch_feature_vector = further_feature_vector[i*batch_length:(i+1)*batch_length]
                full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
            current_batch = i+1
            #print("full shape: ", np.array(train_set).shape)
            #print("full shape: ", np.array(batch_feature_vector).shape) 
            yield full_train_set, y_train[i*batch_length:(i+1)*batch_length]

def minibatch_generator_cosine_local(X_train,y_train,batch_length,w2v_model,sentence_size,vector_comparison,further_feature_vector=None):

    amount_batchs = int(round(len(X_train)/batch_length))
    print("amount batch: ",str(amount_batchs))
    current_batch = 0
    while 1:
         if current_batch == amount_batchs:
             print("current batch: ",str(current_batch))
             train_set = []
             full_train_set = []
             print(str(current_batch*batch_length)+" "+str(len(y_train)))
             for elem in X_train[current_batch*batch_length:len(y_train)]:
                 train_set.append(get_cosine_vector_sentence_local(elem,w2v_model,sentence_size,vector_comparison))
             if further_feature_vector is None:
                 full_train_set = np.array(train_set)
             else:
                 batch_feature_vector = further_feature_vector[current_batch*batch_length:len(y_train)]
                 full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
             y =  y_train[current_batch*batch_length:len(y_train)]
             current_batch = 0
             print("y size: "+str(len(y_train[current_batch*batch_length:len(y_train)])))
             yield full_train_set, y

         for i in range(amount_batchs): # 1875 * 32 = 60000 -> # of training samples
            print("batch number: ",str(i))
            train_set = []
            full_train_set = []
            print(str(i*batch_length)+" "+str((i+1)*batch_length))
            for elem in X_train[i*batch_length:(i+1)*batch_length]:
                train_set.append(get_cosine_vector_sentence_local(elem,w2v_model,sentence_size,vector_comparison))
            if further_feature_vector is None:
                full_train_set = np.array(train_set)
            else:
                batch_feature_vector = further_feature_vector[i*batch_length:(i+1)*batch_length]
                full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
            current_batch = i+1
            #print("full shape: ", np.array(train_set).shape)
            #print("full shape: ", np.array(batch_feature_vector).shape) 
            yield full_train_set, y_train[i*batch_length:(i+1)*batch_length]


def minibatch_generator(X_train,y_train,batch_length,w2v_corpus,sentence_size,further_feature_vector = None):

    amount_batchs = int(round(len(X_train)/batch_length))
    print("amount batch: ",str(amount_batchs))
    current_batch = 0
    while 1:


         if current_batch == amount_batchs:
             print("current batch: ",str(current_batch))
             train_set = []
             full_train_set = []
             print(str(current_batch*batch_length)+" "+str(len(y_train)))
             for elem in X_train[current_batch*batch_length:len(y_train)]:
                 train_set.append(get_distributional_vector_sentence(elem,w2v_corpus, sentence_size))
             if further_feature_vector is None:
                 full_train_set = np.array(train_set)
             else:
                 batch_feature_vector = further_feature_vector[current_batch*batch_length:len(y_train)]
                 full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
             y =  y_train[current_batch*batch_length:len(y_train)]
             current_batch = 0
             print("y size: "+str(len(y_train[current_batch*batch_length:len(y_train)])))
             yield full_train_set, y

         for i in range(amount_batchs): # 1875 * 32 = 60000 -> # of training samples
            print("batch number: ",str(i))
            train_set = []
            full_train_set = []
            print(str(i*batch_length)+" "+str((i+1)*batch_length))
            for elem in X_train[i*batch_length:(i+1)*batch_length]:
                train_set.append(get_distributional_vector_sentence(elem,w2v_corpus, sentence_size))
            if further_feature_vector is None:
                full_train_set = np.array(train_set)
            else:
                batch_feature_vector = further_feature_vector[i*batch_length:(i+1)*batch_length]
                full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
            current_batch = i+1
            #print("full shape: ", np.array(train_set).shape)
            #print("full shape: ", np.array(batch_feature_vector).shape) 
            yield full_train_set, y_train[i*batch_length:(i+1)*batch_length]






def minibatch_generator_local(X_train,y_train,batch_length,w2v_corpus,sentence_size,further_feature_vector = None):

    amount_batchs = int(round(len(X_train)/batch_length))
    print("amount batch: ",str(amount_batchs))
    current_batch = 0
    while 1:
         if current_batch == amount_batchs:
             print("current batch: ",str(current_batch))
             train_set = []
             full_train_set = []
             print(str(current_batch*batch_length)+" "+str(len(y_train)))
             for elem in X_train[current_batch*batch_length:len(y_train)]:
                 train_set.append(get_distributional_vector_sentence_local(elem,w2v_corpus, sentence_size))
             if further_feature_vector is None:
                 full_train_set = np.array(train_set)
             else:
                 batch_feature_vector = further_feature_vector[current_batch*batch_length:len(y_train)]
                 full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
             y =  y_train[current_batch*batch_length:len(y_train)]
             current_batch = 0
#             print("y size: "+str(len(y_train[current_batch*batch_length:len(y_train)])))
             yield full_train_set, y

         for i in range(amount_batchs): # 1875 * 32 = 60000 -> # of training samples
            print("batch number: ",str(i))
            train_set = []
            full_train_set = []
            print(str(i*batch_length)+" "+str((i+1)*batch_length))
            for elem in X_train[i*batch_length:(i+1)*batch_length]:
                train_set.append(get_distributional_vector_sentence_local(elem,w2v_corpus, sentence_size))
            if further_feature_vector is None:
                full_train_set = np.array(train_set)
            else:
                batch_feature_vector = further_feature_vector[i*batch_length:(i+1)*batch_length]
                full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
            current_batch = i+1
            #print("full shape: ", np.array(train_set).shape)
            #print("full shape: ", np.array(batch_feature_vector).shape) 
            yield full_train_set, y_train[i*batch_length:(i+1)*batch_length]

         

def get_token_inquirer_valid_words(sentence,dataframe):
    new_sentence = []
    valid_words = dataframe.index.tolist()
    valid_words = [re.sub('\#[0-9]+','',word) for word in valid_words]
    sentence = string_u.normalize_sentence(sentence)
    sentence = nltk_u.get_lemmatized_tokens(sentence.lower())
    for lemmatized_word in sentence:
        if lemmatized_word in valid_words:
            new_sentence.append(lemmatized_word)
    return new_sentence


def get_token_inquirer_features_valid_words(sentence,max_sentence_size,dataframe, features = 'all'):
    label_vector = []
    contain_sentiment_word = False
    sentence = string_u.normalize_sentence(sentence)
    sentence = nltk_u.get_lemmatized_tokens(sentence.lower())
    feature_token_vector = []
    dicts_id = []
    count = 0
    word_quantity = 0
    from nltk.stem import WordNetLemmatizer
    wordnet_lemmatizer = WordNetLemmatizer()
    new_sentence = []
     
#    print(dataframe.columns.values.tolist()[1:-2])
#    labels_binary_to_id = [] 
    if features == 'all':
        for i,column in enumerate(dataframe.columns.values.tolist()[1:-2]):
             lb = LabelBinarizer()
             label_to_id = [] 
             contain_word = dict()
             count_labels = 0
             for column_elem in dataframe[column].values.tolist():
                #print(contain_word.keys())
                # print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_labels})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             dicts_id.append(contain_word)          
             lb.fit(label_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())

             label_vector.append(lb)
    elif len(features) >= 1:
        for i,feature in enumerate(features):
             lb = LabelBinarizer() 
             label_to_id = [] 

             contain_word =dict()
             count_labels = 0

#             print(feature)
#             print(dataframe[feature].values.tolist())
#             print(dataframe[feature].values.tolist()) 
             for column_elem in dataframe[feature].values.tolist():
     #print(contain_word.keys())
                 #print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_labels})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             dicts_id.append(contain_word)          
             lb.fit(label_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())
#             print(lb.get_params())
             label_vector.append(lb)
    for j, token in enumerate(sentence):
        word_quantity = j 
        token = token.lower().strip()
        lemmatized_word = wordnet_lemmatizer.lemmatize(token, pos='v')
        feature_partial_vector = []
        if j == max_sentence_size:
            return feature_token_vector 
        if lemmatized_word in dataframe.index.tolist():
           contain_sentiment_word = True
           new_sentence.append(lemmatized_word)
            #word_feature_list = dataframe
            #print(word_feature_list)
           for l, column_vector in enumerate(label_vector):
                #print(lemmatized_word)
                 #print(features[l])    
                word_feature_list = dataframe.loc[str(lemmatized_word),str(features[l])]

                #if len(column_vector.classes_.tolist())==2:
                #    lenght = len(column_vector.classes_.tolist())-1
                #else:
                #    lenght = len(column_vector.classes_.tolist())
#                print([labels_binary_to_id[l][word_feature_list]])

                #print(word_feature_list)
                #print(int(dicts_id[l][word_feature_list]))
                result = ''
                ls = dicts_id[l]
                if lemmatized_word in ['noun']:
                    result = ls['missing']
                else:
                    result = ls[word_feature_list]

                feat = column_vector.transform([result])
                for num in feat:
                    feature_partial_vector.append(num[0])
           feature_token_vector.append(feature_partial_vector)       
    if(word_quantity < max_sentence_size):
        for elem in  range(max_sentence_size - word_quantity + 1):
            feature_token_vector.append([-1]*count)
    if(contain_sentiment_word):
        return [],[]
    return new_sentence, feature_token_vector

def get_token_inquirer_features(sentence,max_sentence_size,dataframe, features = 'all'):
    label_vector = []
    feature_token_vector = []
    dicts_id = []
    count = 0
    word_quantity = 0
    from nltk.stem import WordNetLemmatizer
    wordnet_lemmatizer = WordNetLemmatizer() 
#    print(dataframe.columns.values.tolist()[1:-2])
#    labels_binary_to_id = [] 
    if features == 'all':
        for i,column in enumerate(dataframe.columns.values.tolist()[1:-2]):
             lb = LabelBinarizer()
             label_to_id = [] 
                         
             contain_word =dict()
             count_labels = 0
             for column_elem in dataframe[column].values.tolist():
                #print(contain_word.keys())
                # print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_label})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             
             dicts_id.append(contain_word)          
             lb.fit(labels_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())

             label_vector.append(lb)
    elif len(features) >= 1:
        for i,feature in enumerate(features):
             lb = LabelBinarizer() 
             label_to_id = [] 
                         
             contain_word =dict()
             count_labels = 0

#             print(feature)
#             print(dataframe[feature].values.tolist())
#             print(dataframe[feature].values.tolist()) 
             for column_elem in dataframe[feature].values.tolist():
                 #print(contain_word.keys())
                 #print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_labels})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             dicts_id.append(contain_word)          
             lb.fit(label_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())
#             print(lb.get_params())
             label_vector.append(lb)
    for j, token in enumerate(sentence):
        word_quantity = j 
        token = token.lower().strip()
        lemmatized_word = wordnet_lemmatizer.lemmatize(token, pos='v')
        feature_partial_vector = []
        if j == max_sentence_size:
            return feature_token_vector 
        if lemmatized_word in dataframe.index.tolist():
            #word_feature_list = dataframe
            #print(word_feature_list)
            for l, column_vector in enumerate(label_vector):
                #print(lemmatized_word)
                #print(features[l])    
                word_feature_list = dataframe.loc[str(lemmatized_word),str(features[l])]
               
                #if len(column_vector.classes_.tolist())==2:
                #    lenght = len(column_vector.classes_.tolist())-1
                #else:
                #    lenght = len(column_vector.classes_.tolist())
#                print([labels_binary_to_id[l][word_feature_list]])
                
                #print(word_feature_list)
                #print(int(dicts_id[l][word_feature_list]))
                result = ''
                ls = dicts_id[l]
                if lemmatized_word in ['noun']:
                    result = ls['missing']
                else:
                    result = ls[word_feature_list]
                feat = column_vector.transform([result])
                for num in feat:
                    feature_partial_vector.append(num[0])
            feature_token_vector.append(feature_partial_vector)
        else:
            feature_token_vector.append([-1]*count)
    if(word_quantity < max_sentence_size):
        for elem in  range(max_sentence_size - word_quantity + 1):
            feature_token_vector.append([-1]*count)       
    return feature_token_vector

def get_training_stocktwits_json(json_file_path,json_type):
    import json
    training_input = []
    training_output = []

    with open(json_file_path) as file:
        objects = json.load(file)

        if json_type == 'head':
           for object in objects:
               training_input.append(object['title'])
               if  float(object['sentiment']) < 0.0:
                   training_output.append('Bearish')
               else:
                    training_output.append('Bullish')

        elif json_type == 'microblog' :
            for object in objects:
                for spam in object['spans']:
                    training_input.append(spam)
                    if  float(object['sentiment score']) <= 0.0:
                        training_output.append('Bearish')
                    else:
                        training_output.append('Bullish')

    return  np.array(training_input),training_output

