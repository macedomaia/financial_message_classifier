#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Simple Python wrapper for runTagger.sh script for CMU's Tweet Tokeniser and Part of Speech tagger: http://www.ark.cs.cmu.edu/TweetNLP/
# https://raw.githubusercontent.com/ianozsvald/ark-tweet-nlp-python/master/CMUTweetTagger.py

Usage:
results=runtagger_parse(['example tweet 1', 'example tweet 2'])
results will contain a list of lists (one per tweet) of triples, each triple represents (term, type, confidence)


Refactored (somewhat) by Manuela Huerlimann, November 2016
"""
import re
import subprocess
import shlex
import sys

# The only relavent source I've found is here:
# http://m1ked.com/post/12304626776/pos-tagger-for-twitter-successfully-implemented-in
# which is a very simple implementation, my implementation is a bit more
# useful (but not much).

# NOTE this command is directly lifted from runTagger.sh


class CMUTweetTagger: # TODO: refactor this further, possibly into an NLTK-style tagger - e.g. like this: http://www.nltk.org/_modules/nltk/tag/stanford.html

    RUN_TAGGER_CMD = "java -XX:ParallelGCThreads=2 -Xmx500m -jar"
    TAGGER_JAR_FILE = "../../../tools/ark-tweet-nlp-0.3.2/ark-tweet-nlp-0.3.2.jar"

    def __init__(self, run_tagger_cmd=RUN_TAGGER_CMD, tagger_jar = TAGGER_JAR_FILE):
        """Simple test to make sure we can see the script"""
        self.run_command = run_tagger_cmd + " " + tagger_jar
        try:
            args = shlex.split(self.run_command)
            args.append("--help")
            po = subprocess.Popen(args, stdout=subprocess.PIPE)
            # old call - made a direct call to runTagger.sh (not Windows friendly)
            #po = subprocess.Popen([run_tagger_cmd, '--help'], stdout=subprocess.PIPE)
            while not po.poll():
                lines = [str(l) for l in po.stdout]
                # we expected the first line of --help to look like the following:
                assert "RunTagger [options]" in lines[0]

        except (OSError) as err:
            print("Caught an OSError, have you specified the correct path to runTagger.sh? We are using \"%s\". Exception: %r" % (self.run_command, repr(err)))
            sys.exit(1)


    def _split_result(self, row): # TODO: refactor this
        """Parse a tab-delimited returned line, modified from: https://github.com/brendano/ark-tweet-nlp/blob/master/scripts/show.py"""
        try:
            token, tag, confidence = row.split('\\t')
            return (token, tag)  # TODO: put back in? , float(confidence)
        except ValueError:
            return None


    def _clean_raw_results(self, raw_result, delim="<§$!#!$§>"): # TODO: refactor - use temporary file or smth, like NLTK taggers: http://www.nltk.org/_modules/nltk/tag/stanford.html

        pos_result = raw_result.lstrip("b'").rstrip(r"\\n\\n\\'").strip()  # string cleaning
        pos_result = re.sub(r'\\n\\n', delim, pos_result) # put in a message delimiter
        messages = []
        for message in pos_result.split(delim): # split by message delimiter to loop through messages
            msg = []
            for word in message.split('\\n'): # get at individual words by splitting on newslines
                result = self._split_result(word.strip())
                if result is not None:
                    msg.append(result)

            if msg != []:
                messages.append(msg)

        return messages


    def _call_runtagger(self, tweets):
        """Call runTagger.sh using a named input file"""

        # remove carriage returns as they are tweet separators for the stdin
        # interface
        tweets_cleaned = [tw.replace('\n', ' ') for tw in tweets]
        message = "\n".join(tweets_cleaned)

        # force UTF-8 encoding (from internal unicode type) to avoid .communicate encoding error as per:
        # http://stackoverflow.com/questions/3040101/python-encoding-for-pipe-communicate
        message = message.encode('utf-8')

        # build a list of args
        args = shlex.split(self.run_command)
        args.append('--output-format')
        args.append('conll')
        po = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # old call - made a direct call to runTagger.sh (not Windows friendly)
        #po = subprocess.Popen([run_tagger_cmd, '--output-format', 'conll'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        analysed, status = po.communicate(message)
        # expect a tuple of 2 items like:
        # ('hello\t!\t0.9858\nthere\tR\t0.4168\n\n',
        # 'Listening on stdin for input.  (-h for help)\nDetected text input format\nTokenized and tagged 1 tweets (2 tokens) in 7.5 seconds: 0.1 tweets/sec, 0.3 tokens/sec\n')
        return self._clean_raw_results(str(analysed))


    def tag(self, tweet):
       pass
        # TODO: make it possible to tag a single string tweet also ;)

    def tag_sents(self, tweets):
        """Call runTagger.sh on a list of tweets, parse the result, return lists of tuples of (term, type, confidence)"""
        return self._call_runtagger(tweets)

if __name__ == "__main__":
    tagger = CMUTweetTagger()
    print("Checking that we can see \"%s\", this will crash if we can't" % (tagger.RUN_TAGGER_CMD))
    tweets = ['this is a message', 'and a second message']
    print(tagger.tag_sents(tweets))