__author__ = 'macedo'

from myutils.nltk_util import get_lemmatized_tokens,get_sent_tokenized, get_pos_tags, penn_to_wn
from myutils.string_util import normalize_sentence
import myutils.nltk_util as nltk_u
import myutils.string_util as string_u
from sklearn.preprocessing import LabelBinarizer
from gensim import models
import numpy as np
import re

def get_tokenized_sentences_and_classes(sentences):
#    tokenized_sentences = []
   
#    classes = []
#    max_sentence_size = 0
#    for i,sentence in enumerate(sentences):
#        splited_line = normalize_sentence(sentence).split("\t")
    #if i > 2000:
    #    break
#       print(splited_line)
#        if len(splited_line) == 2:
#            tokenized_sentence = get_lemmatized_tokens(splited_line[0])
#            tokenized_sentence = splited_line[0].split()
#            tokenized_sentences.append(tokenized_sentence)
#            if max_sentence_size < len(tokenized_sentence):
#                    max_sentence_size = len(tokenized_sentence)
#            classes.append(splited_line[1])
    tokenized_sentences = []
    classes = []
    max_sentence_size = 0
#    with open('/home/sousa/stock_datased_balanced.txt', 'a') as f:
#    f.write('hi there\n') # python will convert \n to os.linesep
#f.close() # you can omit in most cases as the destructor will call it
    for i,sentence in enumerate(sentences):
         
#            f.write(sentence[0]+'\t\t'+sentence[1]+'\n')
         normalized_sentence = normalize_sentence(sentence[0])

         if i > 50:
             break
#        print(splited_line)

         tokenized_sentence = get_sent_tokenized(normalized_sentence)
#        tokenized_sentence = normalized_sentence.split()
         tokenized_sentences.append(tokenized_sentence)
         if max_sentence_size < len(tokenized_sentence):
             max_sentence_size = len(tokenized_sentence)
         classes.append(sentence[1])
            #f.write(sentence[0]+'@'+sentence[1]) # python will convert \n to os.linesep
#    f.close()
    
    return tokenized_sentences, classes, max_sentence_size

def get_lines(files):
    lines = []
    for file in files:
        with open(file) as f:
            for line in f:
                lines.append(line)
    return lines

def generate_w2v_model(sentences):
    return models.Word2Vec(sentences, min_count=1,size=300)

def ordering_vector(vector):
    ordered_vector = None
    if not(vector is None):
        ordered_vector = [0] * 300
        for lem in vector:
            ordered_vector[int(lem)] = vector[lem]

    return ordered_vector


def get_vector(corpus, word):
    import requests
    import json
    import operator

    url = 'http://alphard.fim.uni-passau.de:8996/vectors'

    terms = [word]

    data = {'corpus': corpus,
            'model': 'W2V',
            'language': 'EN',
            'terms': terms}

    headers = {
            'accept': "application/json",
            'content-type': "application/json",
            'cache-control': "no-cache"
        }

    res = requests.post(url, data=json.dumps(data), headers=headers)
    res.raise_for_status()
    vector = ordering_vector(res.json()['terms'][word])
    return vector


def get_distributional_vector_sentence(tokenized_sentence,w2v_corpus,max_sentence_size):
    in_tuple = (max_sentence_size,300)
    w2v_tokenized_sentence = np.empty(in_tuple, dtype= 'float')
    w2v_tokenized_sentence.fill(-1.0)

    for i,token in enumerate(tokenized_sentence):
        if max_sentence_size <= i:
           return w2v_tokenized_sentence
        token_vector = get_vector(w2v_corpus,token)
        if not(token_vector is None):
           vec = token_vector
#           print(vec)
        else:
           vec = [-1.0] * 300
        w2v_tokenized_sentence[i] = vec
    return w2v_tokenized_sentence

def get_distributional_vector_sentences(tokenized_sentences,w2v_corpus,max_sentence_size):
    print("distributional sentences actived")

    in_tuple = (len(tokenized_sentences), max_sentence_size,300)
    
    in_vectors = np.empty(in_tuple,dtype=np.float64)
    in_vectors.fill(-1.0)

    for i,tokenized_sentence in enumerate(tokenized_sentences):
        in_vectors[i] = get_distributional_vector_sentence(tokenized_sentence,w2v_corpus,max_sentence_size)
    return in_vectors


def get_pos_sentences(sentences,max_sentence_size):
    in_tuple = (len(sentences), max_sentence_size,1)
    pos_sentences = np.empty(in_tuple,dtype=object)
    pos_sentences.fill(None)

    for i,sentence in enumerate(sentences):
        splited_line = normalize_sentence(sentence).split("\t")
        tokenized_sentence = get_lemmatized_tokens(splited_line[0])

        pos_sentence =  get_pos_tags(splited_line[0])
        for j,pos in enumerate(pos_sentence):
            pos_sentences[i][j] = penn_to_wn(pos_sentence[j])


        if len(tokenized_sentence) != len(pos_sentence):
                print("error")
                print(splited_line[0])
                print(len(tokenized_sentence))
                print(len(pos_sentence))

    return pos_sentences


def load_w2v_model(model_path, extension):
    if extension is "bin":
        return models.Word2Vec.load_word2vec_format(fname=model_path,binary=True)
    return models.Word2Vec.load_word2vec_format(fname=model_path)

def minibatch_generator(X_train,y_train,batch_length,w2v_corpus,sentence_size,further_feature_vector = None):

    amount_batchs = int(round(len(X_train)/batch_length))
    print("amount batch: ",str(amount_batchs))
    current_batch = 0
    while 1:
         if current_batch == amount_batchs:
             print("current batch: ",str(current_batch))
             train_set = []
             full_train_set = []
             print(str(current_batch*batch_length)+" "+str(len(y_train)))
             for elem in X_train[current_batch*batch_length:len(y_train)]:
                 train_set.append(get_distributional_vector_sentence(elem,w2v_corpus, sentence_size))
             if further_feature_vector is None:
                 full_train_set = np.array(train_set)
             else:
                 batch_feature_vector = further_feature_vector[current_batch*batch_length:len(y_train)]
                 full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
             y =  y_train[current_batch*batch_length:len(y_train)]
             current_batch = 0
             print("y size: "+str(len(y_train[current_batch*batch_length:len(y_train)])))
             yield full_train_set, y

         for i in range(amount_batchs): # 1875 * 32 = 60000 -> # of training samples
            print("batch number: ",str(i))
            train_set = []
            full_train_set = []
            print(str(i*batch_length)+" "+str((i+1)*batch_length))
            for elem in X_train[i*batch_length:(i+1)*batch_length]:
                train_set.append(get_distributional_vector_sentence(elem,w2v_corpus, sentence_size))
            if further_feature_vector is None:
                full_train_set = np.array(train_set)
            else:
                batch_feature_vector = further_feature_vector[i*batch_length:(i+1)*batch_length]
                full_train_set = [np.array(train_set),np.array(batch_feature_vector)]
            current_batch = i+1
            #print("full shape: ", np.array(train_set).shape)
            #print("full shape: ", np.array(batch_feature_vector).shape) 
            yield full_train_set, y_train[i*batch_length:(i+1)*batch_length]
         

def get_token_inquirer_valid_words(sentence,dataframe):
    new_sentence = []
    valid_words = dataframe.index.tolist()
    valid_words = [re.sub('\#[0-9]+','',word) for word in valid_words]
    sentence = string_u.normalize_sentence(sentence)
    sentence = nltk_u.get_lemmatized_tokens(sentence.lower())
    for lemmatized_word in sentence:
        if lemmatized_word in valid_words:
            new_sentence.append(lemmatized_word)
    return new_sentence


def get_token_inquirer_features_valid_words(sentence,max_sentence_size,dataframe, features = 'all'):
    label_vector = []
    contain_sentiment_word = False
    sentence = string_u.normalize_sentence(sentence)
    sentence = nltk_u.get_lemmatized_tokens(sentence.lower())
    feature_token_vector = []
    dicts_id = []
    count = 0
    word_quantity = 0
    from nltk.stem import WordNetLemmatizer
    wordnet_lemmatizer = WordNetLemmatizer()
    new_sentence = []
     
#    print(dataframe.columns.values.tolist()[1:-2])
#    labels_binary_to_id = [] 
    if features == 'all':
        for i,column in enumerate(dataframe.columns.values.tolist()[1:-2]):
             lb = LabelBinarizer()
             label_to_id = [] 
             contain_word = dict()
             count_labels = 0
             for column_elem in dataframe[column].values.tolist():
                #print(contain_word.keys())
                # print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_labels})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             dicts_id.append(contain_word)          
             lb.fit(label_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())

             label_vector.append(lb)
    elif len(features) >= 1:
        for i,feature in enumerate(features):
             lb = LabelBinarizer() 
             label_to_id = [] 

             contain_word =dict()
             count_labels = 0

#             print(feature)
#             print(dataframe[feature].values.tolist())
#             print(dataframe[feature].values.tolist()) 
             for column_elem in dataframe[feature].values.tolist():
     #print(contain_word.keys())
                 #print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_labels})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             dicts_id.append(contain_word)          
             lb.fit(label_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())
#             print(lb.get_params())
             label_vector.append(lb)
    for j, token in enumerate(sentence):
        word_quantity = j 
        token = token.lower().strip()
        lemmatized_word = wordnet_lemmatizer.lemmatize(token, pos='v')
        feature_partial_vector = []
        if j == max_sentence_size:
            return feature_token_vector 
        if lemmatized_word in dataframe.index.tolist():
           contain_sentiment_word = True
           new_sentence.append(lemmatized_word)
            #word_feature_list = dataframe
            #print(word_feature_list)
           for l, column_vector in enumerate(label_vector):
                #print(lemmatized_word)
                 #print(features[l])    
                word_feature_list = dataframe.loc[str(lemmatized_word),str(features[l])]

                #if len(column_vector.classes_.tolist())==2:
                #    lenght = len(column_vector.classes_.tolist())-1
                #else:
                #    lenght = len(column_vector.classes_.tolist())
#                print([labels_binary_to_id[l][word_feature_list]])

                #print(word_feature_list)
                #print(int(dicts_id[l][word_feature_list]))
                result = ''
                ls = dicts_id[l]
                if lemmatized_word in ['noun']:
                    result = ls['missing']
                else:
                    result = ls[word_feature_list]

                feat = column_vector.transform([result])
                for num in feat:
                    feature_partial_vector.append(num[0])
           feature_token_vector.append(feature_partial_vector)       
    if(word_quantity < max_sentence_size):
        for elem in  range(max_sentence_size - word_quantity + 1):
            feature_token_vector.append([-1]*count)
    if(contain_sentiment_word):
        return [],[]
    return new_sentence, feature_token_vector

def get_token_inquirer_features(sentence,max_sentence_size,dataframe, features = 'all'):
    label_vector = []
    feature_token_vector = []
    dicts_id = []
    count = 0
    word_quantity = 0
    from nltk.stem import WordNetLemmatizer
    wordnet_lemmatizer = WordNetLemmatizer() 
#    print(dataframe.columns.values.tolist()[1:-2])
#    labels_binary_to_id = [] 
    if features == 'all':
        for i,column in enumerate(dataframe.columns.values.tolist()[1:-2]):
             lb = LabelBinarizer()
             label_to_id = [] 
                         
             contain_word =dict()
             count_labels = 0
             for column_elem in dataframe[column].values.tolist():
                #print(contain_word.keys())
                # print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_label})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             
             dicts_id.append(contain_word)          
             lb.fit(labels_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())

             label_vector.append(lb)
    elif len(features) >= 1:
        for i,feature in enumerate(features):
             lb = LabelBinarizer() 
             label_to_id = [] 
                         
             contain_word =dict()
             count_labels = 0

#             print(feature)
#             print(dataframe[feature].values.tolist())
#             print(dataframe[feature].values.tolist()) 
             for column_elem in dataframe[feature].values.tolist():
                 #print(contain_word.keys())
                 #print(column_elem)
                 if not(column_elem in list(contain_word.keys())):
                    contain_word.update({column_elem : count_labels})
                    label_to_id.append(count_labels)
                    count_labels += 1
                 else:
                    label_to_id.append(contain_word[column_elem]) 
             dicts_id.append(contain_word)          
             lb.fit(label_to_id)
             if len(lb.classes_.tolist())==2:
                 count = count + 1
             elif len(lb.classes_.tolist())>2:
                 count = count + len(lb.classes_.tolist())
#             print(lb.get_params())
             label_vector.append(lb)
    for j, token in enumerate(sentence):
        word_quantity = j 
        token = token.lower().strip()
        lemmatized_word = wordnet_lemmatizer.lemmatize(token, pos='v')
        feature_partial_vector = []
        if j == max_sentence_size:
            return feature_token_vector 
        if lemmatized_word in dataframe.index.tolist():
            #word_feature_list = dataframe
            #print(word_feature_list)
            for l, column_vector in enumerate(label_vector):
                #print(lemmatized_word)
                #print(features[l])    
                word_feature_list = dataframe.loc[str(lemmatized_word),str(features[l])]
               
                #if len(column_vector.classes_.tolist())==2:
                #    lenght = len(column_vector.classes_.tolist())-1
                #else:
                #    lenght = len(column_vector.classes_.tolist())
#                print([labels_binary_to_id[l][word_feature_list]])
                
                #print(word_feature_list)
                #print(int(dicts_id[l][word_feature_list]))
                result = ''
                ls = dicts_id[l]
                if lemmatized_word in ['noun']:
                    result = ls['missing']
                else:
                    result = ls[word_feature_list]
                feat = column_vector.transform([result])
                for num in feat:
                    feature_partial_vector.append(num[0])
            feature_token_vector.append(feature_partial_vector)
        else:
            feature_token_vector.append([-1]*count)
    if(word_quantity < max_sentence_size):
        for elem in  range(max_sentence_size - word_quantity + 1):
            feature_token_vector.append([-1]*count)       
    return feature_token_vector

def get_training_stocktwits_json(json_file_path,json_type):
    import json
    training_input = []
    training_output = []

    with open(json_file_path) as file:
        objects = json.load(file)

        if json_type == 'head':
           for object in objects:
               training_input.append(object['title'])
               if  float(object['sentiment']) < 0.0:
                   training_output.append('Bearish')
               else:
                    training_output.append('Bullish')

        elif json_type == 'microblog' :
            for object in objects:
                for spam in object['spans']:
                    training_input.append(spam)
                    if  float(object['sentiment score']) <= 0.0:
                        training_output.append('Bearish')
                    else:
                        training_output.append('Bullish')

    return  np.array(training_input),training_output

