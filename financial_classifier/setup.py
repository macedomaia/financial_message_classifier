# -*- coding: utf-8 -*-

import sys
if sys.version_info.major < 3:
    sys.exit("Sorry, Only Python 3 is supported.")

VERSION = '0.1.0.dev0'

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open('requirements.txt', 'r') as f:
    _install_requires = [line.rstrip('\n') for line in f]

setup(
    name="ssix_financial_classifier",
    version=VERSION,
    packages=['ssix'],
    platforms=['Linux', 'Unix', 'Mac OS-X'],
    install_requires=_install_requires,
    tests_require=[
        'nose'
    ],
    test_suite="nose.collector"
)
