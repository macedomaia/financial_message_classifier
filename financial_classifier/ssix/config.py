# -*- coding: utf-8 -*-

from os.path import dirname, abspath, join

import yaml


class DotDict(dict):
    """
    a dictionary that supports dot notation 
    as well as dictionary access notation 
    usage: d = DotDict() or d = DotDict({'val1':'first'})
    set attributes: d.val2 = 'second' or d['val2'] = 'second'
    get attributes: d.val2 or d['val2']
    
    source: http://stackoverflow.com/questions/13520421/recursive-dotdict
    
    """
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self, dct):
        for key, value in dct.items():
            if hasattr(value, 'keys'):
                value = DotDict(value)
            self[key] = value


def load(configuration):
    if not configuration:
        raise ValueError("Invalid configuration: {}".format(configuration))
    return DotDict(yaml.load(configuration))


def load_from_file(filepath):
    with open(filepath, 'r') as f:
        return load(f)


def load_defaults():
    default_cfg_path = join(abspath(dirname(__file__)), "resources", "config.yaml")
    return load_from_file(default_cfg_path)
