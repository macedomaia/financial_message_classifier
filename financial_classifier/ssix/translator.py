# -*- coding: utf-8 -*-

import logging


class _Translator(object):
    _logger = logging.getLogger(__name__)

    def translate(self, text, from_language):
        raise NotImplementedError


class GeofluentTranslator(_Translator):

    def __init__(self, geofluent):
        self._logger.info("Initializing Geofluent translator...")
        self.__geofluent = geofluent
        self.__languages = geofluent.languages()
        self._logger.info("languages=%s", self.__languages)

    def translate(self, text, from_language):
        self._logger.info("Translating '%s' from '%s'", text, from_language)
        from_code = next((l for l in self.__languages if l[0].startswith(from_language) and l[1] == 'en-xn'), None)
        if from_code is None:
            raise ValueError("Unsupported translation from {}".format(from_language))
        return self.__geofluent.translate(text, *from_code)


def create_geofluent_translator(config):
    from geofluent import GeoFluent
    cfg = config.ssix.translator.geofluent
    return GeofluentTranslator(GeoFluent(key=cfg.key, html_unescape=True, secret=cfg.secret))
