# -*- coding: utf-8 -*-


class _Language(object):

    def __init__(self, name, code):
        if not name or not code:
            raise ValueError("Invalid arguments for a new Language.")
        self.name = name
        self.code = code

    def __str__(self):
        return "{} [{}]".format(self.name, self.code)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(self.name) ^ hash(self.code)


German = _Language('german', 'de')
English = _Language('english', 'en')
Spanish = _Language('spanish', 'es')

supported_langs = {English, German, Spanish}


def __check_supported_langs(lang):
    if not (lang in supported_langs):
        raise RuntimeError("Unsupported language {}".format(lang))


def parselang(name):
    return next((l for l in supported_langs if l.name == name or l.code == name), None)


def create_rule_analyzer(language):
    __check_supported_langs(language)
    if language != English:
        raise NotImplementedError("Rule Analyzer not available yet for {}".format(language))

    # TODO: Vader default is for english. Needs customization for the other languages
    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    return SentimentIntensityAnalyzer()


def create_aspect_analyzer(language, config, word_embedding_model):
    __check_supported_langs(language)
    if language != English:
        raise NotImplementedError("Aspect Analyzer not available yet for {}".format(language))

    from ssix.aspect.extractor import AspectExpressionExtractor
    return AspectExpressionExtractor(config, word_embedding_model)


def create_predictor_analyzer(language, word_embedding_model):
    __check_supported_langs(language)
    if language != English:
        raise NotImplementedError("Aspect Analyzer not available yet for {}".format(language))

    from ssix.models import create_predictor_model
    from ssix.predictor import MicroblogPredictor
    return MicroblogPredictor(word_embedding_model, create_predictor_model(language))


def create_analyzed_sentence(language, text):
    __check_supported_langs(language)
    if language != English:
        raise NotImplementedError("Sentence Analyzer not available yet for {}".format(language))

    from ssix.sentence import EnglishSentence
    return EnglishSentence(text)
