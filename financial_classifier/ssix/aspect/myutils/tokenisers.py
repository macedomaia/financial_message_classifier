from nltk.tokenize.api import StringTokenizer

from .twokenize import tokenizeRawTweetText


class Nokeniser(object):
    """
        Don't tokenise, just return text as is.
    """

    def __init__(self):
        pass

    def tokenize(self, s):
        return s


class TwokeniseTokeniser(object):
    """
        Use Python port of Twokenizer () to tokenise tweets
    """

    def __init__(self):
        pass

    def tokenize(self, s):
        return tokenizeRawTweetText(s)


class LowercaseTwokeniseTokeniser(TwokeniseTokeniser):
    """
        Use Python port of Twokenizer () to tokenise tweets & lowercase the resulting string
    """

    def tokenize(self, s):
        return [t.lower() for t in TwokeniseTokeniser.tokenize(self, s)]


class SpaceWordTokeniser(StringTokenizer):
    """
        Split words on single spaces
    """

    def __init__(self):
        self._string = " "

    def tokenize(self, s):
        return [t for t in s.split(self._string) if s not in ['', ' ']]


class LowercaseSpaceWordTokeniser(SpaceWordTokeniser):
    """
        Split words on single spaces and lowercase the result
    """

    def tokenize(self, s):
        return [t.lower() for t in SpaceWordTokeniser.tokenize(self, s)]


class NewlineSentTokeniser(StringTokenizer):
    """
        Split words on single spaces
    """

    def __init__(self):
        self._string = "\n"

    def sent_tokenize(self, s):
        return s.split(self._string)
