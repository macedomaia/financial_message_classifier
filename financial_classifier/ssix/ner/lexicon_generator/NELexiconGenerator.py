# -*- coding: utf-8 -*-

from os import path

import enchant
import collections


class NELexiconGenerator(object):
    __dict = enchant.Dict("en_US")

    def __init__(self):
        self.__suffixes = self.__read_suffixes()

    def generate(self, tofile):
        names_and_symbols = self.__read_companies('NYSE.csv')
        names_and_symbols.update(self.__read_companies('NASDAQ.csv'))
        names_and_symbols.update(self.__read_companies('AMEX.csv'))

        temp = {}
        for name, symbol in names_and_symbols.items():
            temp.update(self.__get_stripped_name_variants(name, symbol))

        names_and_symbols.update(temp)

        ordered_dict = collections.OrderedDict(sorted(names_and_symbols.items()))
        with open(tofile, 'w') as f:
            for name, symbol in ordered_dict.items():
                f.write(name + ' ; ' + symbol + '\n')

    @staticmethod
    def __read_suffixes():
        with open(path.join(path.dirname(path.realpath(__file__)), "suffix.txt"), 'r') as f:
            return [line.strip() for line in f]

    def __get_stripped_name_variants(self, name, symbol):
        company_name = name.strip()
        result = {}
        for i in range(0, 2):
            for suffix in self.__suffixes:
                if company_name.endswith(suffix):
                    company_name = company_name.replace(suffix, '')
                    if not self.__dict.check(symbol) and not self.__dict.check(company_name):
                        result[symbol] = symbol
                        result[company_name] = symbol

        result['$' + symbol] = symbol

        return result

    @staticmethod
    def __read_companies(filename):
        names_and_symbols = {}
        with open(filename) as f:
            for line in f:
                fields = line.lower().replace('"', '').split(',')
                names_and_symbols[fields[1].strip()] = fields[0].strip()

        return names_and_symbols


if __name__ == '__main__':
    # TODO: This is not a comprehensive lexicon for companies. Needs improvement.
    generator = NELexiconGenerator()
    generator.generate('company_lexicon.txt')
