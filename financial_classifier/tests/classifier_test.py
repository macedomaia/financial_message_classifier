# -*- coding: utf8 -*-
# !/usr/bin/python


import unittest

from ssix.classifier import SSIXClassifier
from ssix.analyzers import English
from ssix.config import load_defaults

_cfg = load_defaults()

# Overriding defaults to make sense on development environment
_cfg.ssix.models.word_embeddings.english.path = \
    "./moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin"

_cfg.ssix.aspects.external_tools.root_path = "../../tools"


class SSIXFinancialClassifierTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.classifier = SSIXClassifier(English, _cfg)

    def testSentiment1(self):
        sentiment, targets, aspects = self.classifier.classify("$AAPL it was fun playing with y'all. Long AAPL")
        self.assertAlmostEqual(sentiment, 0.6, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("aapl" in targets.keys())
        self.assertAlmostEqual(targets["aapl"]["sentiment"], 0.6, delta=0.1)
        self.assertEqual(targets["aapl"]["label"], "$aapl")

    def testSentiment2(self):
        sentiment, targets, aspects = \
            self.classifier.classify("$IBM IBM Expands Cloud Datacenters in the UK to Bolster Growth")
        self.assertAlmostEqual(sentiment, 0.4, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("ibm" in targets.keys())
        self.assertAlmostEqual(targets["ibm"]["sentiment"], 0.4, delta=0.1)
        self.assertEqual(targets["ibm"]["label"], "$ibm")

    def testSentiment3(self):
        sentiment, targets, aspects = self.classifier.classify(
            "Goldman's list of shorts mattering most to hedge funds $XOM $IBM $T $CAT $PG $FCX $DIS")
        self.assertAlmostEqual(sentiment, 0.1, delta=0.1)
        self.assertTrue(len(targets) > 2)
        self.assertTrue("ibm" in targets.keys())
        self.assertAlmostEqual(targets["ibm"]["sentiment"], 0.1, delta=0.1)
        self.assertEqual(targets["ibm"]["label"], "$ibm")

    def testSentiment4(self):
        sentiment, targets, aspects = self.classifier.classify(
            "Estimize revenue expectations are 0.55% lower than that of Wall Street for $LNKD Q4")
        self.assertAlmostEqual(sentiment, -0.3, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("lnkd" in targets.keys())
        self.assertAlmostEqual(targets["lnkd"]["sentiment"], -0.3, delta=0.1)
        self.assertEqual(targets["lnkd"]["label"], "$lnkd")


if __name__ == "__main__":
    unittest.main()
