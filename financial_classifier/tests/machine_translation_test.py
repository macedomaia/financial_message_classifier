# -*- coding: utf8 -*-

import unittest

from ssix.config import load_defaults
from ssix.translator import create_geofluent_translator


class GeofluentTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.gf = create_geofluent_translator(load_defaults())

    def test_german(self):
        translated = self.gf.translate("Ich werde es testen", "de")
        self.assertEqual(translated, "I will test it")

    def test_portuguese(self):
        translated = self.gf.translate("Eu testarei", "pt")
        self.assertEqual(translated, "I'll test")

    def test_romanian(self):
        translated = self.gf.translate("Eu va testa", "ro")
        self.assertEqual(translated, "I will test")

    def test_spanish(self):
        translated = self.gf.translate("Lo probaré", "es")
        self.assertEqual(translated, "I'll try it")
