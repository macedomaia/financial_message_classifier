# -*- coding: utf-8 -*-

import unittest

from ssix.sentence import EnglishSentence


class EnglishSubordinationTestCase(unittest.TestCase):

    def test_subordination1(self):
        original = "IBM expands cloud datacenters in the UK but Intel has the worst year in its history."
        s = EnglishSentence(original)
        self.assertEqual(s.sentence, original)
        self.assertTrue(s.has_polarized_subordination())
        self.assertEqual(s.main_clause, "IBM expands cloud datacenters in the UK")
        self.assertEqual(s.subordinate_clause, "Intel has the worst year in its history.")


if __name__ == "__main__":
    unittest.main()
