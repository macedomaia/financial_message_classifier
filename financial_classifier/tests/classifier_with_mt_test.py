# -*- coding: utf8 -*-

import unittest

from ssix.classifier import SSIXClassifierWithTranslation
from ssix.config import load_defaults

_cfg = load_defaults()
_cfg.ssix.models.word_embeddings.english.type = 'dummy'
_cfg.ssix.aspects.external_tools.root_path = "../../tools"


class MachineTranslationTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.classifier = SSIXClassifierWithTranslation(_cfg)

    def test_from_portuguese(self):
        sentiment, targets, aspects = self.classifier.classify("$AAPL foi divertido brincar com vocês.", "pt")
        self.assertAlmostEqual(sentiment, 0.6, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("aapl" in targets.keys())
        self.assertAlmostEqual(targets["aapl"]["sentiment"], 0.6, delta=0.1)

    def test_from_spanish(self):
        sentiment, targets, aspects = self.classifier.classify("$AAPL fue divertido jugar con ustedes.", "es")
        self.assertAlmostEqual(sentiment, 0.6, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("aapl" in targets.keys())
        self.assertAlmostEqual(targets["aapl"]["sentiment"], 0.6, delta=0.1)

    def test_from_romanian(self):
        sentiment, targets, aspects = self.classifier.classify("$AAPL a fost distractiv joc cu voi toți.", "ro")
        self.assertAlmostEqual(sentiment, 0.6, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("aapl" in targets.keys())
        self.assertAlmostEqual(targets["aapl"]["sentiment"], 0.6, delta=0.1)

    def test_from_german(self):
        sentiment, targets, aspects = self.classifier.classify("$AAPL Es hat Spaß gemacht, mit dir zu spielen.", "de")
        self.assertAlmostEqual(sentiment, 0.6, delta=0.1)
        self.assertEqual(len(targets), 1)
        self.assertTrue("aapl" in targets.keys())
        self.assertAlmostEqual(targets["aapl"]["sentiment"], 0.6, delta=0.1)

if __name__ == "__main__":
    unittest.main()
